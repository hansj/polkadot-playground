// Import the API, Keyring and some utility functions
const { ApiPromise, WsProvider } = require('@polkadot/api');
const { Keyring } = require('@polkadot/keyring');

const TO = '5GxEcL18iEegj4wXbTnGD1w1BS2hr2N8we6box1ceEqmB6rG';

async function main () {
  // Instantiate the API
  const wsProvider = new WsProvider('wss://westend-rpc.polkadot.io');
  const api = await ApiPromise.create({ provider: wsProvider });


  // Constuct the keying after the API (crypto has an async init)
  const keyring = new Keyring({ type: 'sr25519' });

  // Add from to our keyring with a hard-deived path (empty phrase, so uses dev)
  const from = keyring.addFromUri('//cryptrendo');

  // Create a extrinsic, transferring 12345 units to TO
  const transfer = api.tx.balances.transfer(TO, 0.00001);


  // Sign and send the transaction using our account
  const hash = await transfer.signAndSend(from);
  
  console.log('Transfer sent with hash', hash.toHex());
}

main().catch(console.error).finally(() => process.exit());